@extends('admin.layouts.guest')

@section('title', 'Login')

@section('content')
<div class="card-body">
    <h1>Login</h1>
    <form method="post">
        @csrf

        <div class="input-group mb-3 has-feedback">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="fa fa-user"></i>
                </span>
            </div>

            <input type="text" name="username" class="form-control" placeholder="Username">
        </div>

        <div class="input-group mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="fa fa-lock"></i>
                </span>
            </div>

            <input type="password" name="password" class="form-control" placeholder="senha">
        </div>

        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember">Lembrar
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <button type="submit" class="btn btn-primary px-4">
                   Entrar
                </button>
            </div>

            <div class="col-6 text-right">
                <a href="{{ route('admin.forgot_password') }}" class="btn btn-link px-0">
                    Esqueceu a senha?
                </a>
            </div>
        </div>
    </form>
</div>
@endsection

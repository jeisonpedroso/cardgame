@extends('admin.layouts.app', ['page' => 'cards'])

@section('title', 'Editar conteúdo inicial')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Adicionar novo personagem
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('admin.home.update',$home_contents->id) }}">
        @method('put')
        @csrf
        <div class="form-group">
            <label for="title_game">Titulo do jogo</label>
            <input type="text" class="form-control"
                   name="title_game"
                   id="title_game"
                   required
                   value="{{old('title',$home_contents->title_game)}}"
            >
        </div>
        <div class="form-group">
            <label for="phrase">Frase inicio</label>
            <textarea class="form-control"
                name="phrase"
                id="phrase"
                required
                placeholder="Frase inicial"
            >{{ old('text_person',$home_contents->phrase) }}</textarea>
        </div>
        <div class="form-group">
            <label for="form_desc">Descricao do formulário de contato</label>
            <textarea class="form-control"
                      name="form_desc"
                      id="form_desc"
                      required
                      placeholder="Descrição do formulário"
            >{{ old('text_person',$home_contents->form_desc) }}</textarea>
        </div>
       <div class="row">
           <div class="col-md-6">
               <div class="form-group">
                   <div class="row">
                       <img style="width: 150px; height: auto" src="{{url('/storage/'.$home_contents->image_background)}}" alt="">
                   </div>
                   <label for="image_background">Imagem do background </label>
                   <input type="file" class="form-control"
                          name="image_background"
                          id="image_background"
                   >
               </div>
           </div>
           <div class="col-md-6">
               <div class="form-group">
                   <div class="row">
                       <img style="width: 150px; height: auto" src="{{url('/storage/'.$home_contents->theme_spotlight)}}" alt="">
                   </div>
                   <label for="theme_spotlight">Imagem de destaque</label>
                   <input type="file" class="form-control"
                          name="theme_spotlight"
                          id="theme_spotlight"
                   >
               </div>
           </div>
       </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Atualizar
            </button>
        </div>
    </form>
</div>
@endsection

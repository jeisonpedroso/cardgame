@extends('admin.layouts.app', ['page' => ''])

@section('title', 'Profile')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Atualizar perfil
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form method="post">
        @csrf

        <div class="form-group">
            <label for="name">Nome</label>
            <input type="text"
                name="name"
                class="form-control"
                id="name"
                placeholder="Name"
                value="{{ old('name', $admin->name) }}"
            >
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input type="email"
                name="email"
                class="form-control"
                id="email"
                placeholder="Email address"
                value="{{ old('email', $admin->email) }}"
            >
        </div>

        <div class="form-group">
            <label for="username">Username</label>
            <input type="text"
                name="username"
                class="form-control"
                id="username"
                placeholder="Username"
                value="{{ old('username', $admin->username) }}"
            >
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
               Atualizar perfil
            </button>
        </div>
    </form>
</div>

{{-- Password update --}}
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
           Alterar senha
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form method="post" action="{{ route('admin.password_update') }}">
        @csrf

        <div class="form-group">
            <label for="current-password">Senha atual</label>
            <input type="password"
                name="current_password"
                class="form-control"
                id="current-password"
                placeholder="Senha atual"
                pattern=".{6,}"
                title="6 caracteres min"
            >
        </div>

        <div class="form-group">
            <label for="password">Nova senha</label>
            <input type="password"
                name="password"
                class="form-control"
                id="password"
                placeholder="Nova senha"
                pattern=".{6,}"
                title="6 caracteres min"
            >
        </div>

        <div class="form-group">
            <label for="confirm-password">Confirme nova senha</label>
            <input type="password"
                name="password_confirmation"
                class="form-control"
                id="confirm-password"
                placeholder="Confirme nova senha"
                pattern=".{6,}"
                title="6 caracteres min"
            >
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Alterar senha
            </button>
        </div>
    </form>
</div>
@endsection
@extends('admin.layouts.app', ['page' => 'cards'])

@section('title', 'Adicionar novo card')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Adicionar novo personagem
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('admin.cards.store') }}">
        @csrf
        <div class="form-group">
            <label for="title">Titulo do personagem</label>
            <input type="text" class="form-control"
                   name="title"
                   id="title"
                   required
            >
        </div>
        <div class="form-group">
            <label for="text_person">Texto do personagem</label>
            <textarea class="form-control"
                name="text_person"
                id="text_person"
                required
                placeholder="Texto do personagem"
            >{{ old('text_person') }}</textarea>
        </div>

        <div class="form-group">
            <label for="image">Imagem do personagem</label>
            <input type="file" class="form-control"
                   name="image"
                   required
            >
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Cadastrar
            </button>
        </div>
    </form>
</div>
@endsection

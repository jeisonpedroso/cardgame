@extends('admin.layouts.app', ['page' => 'cards'])

@section('title', 'Editar card')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Adicionar novo personagem
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('admin.cards.update',$card->id) }}">
        @method('put')
        @csrf
        <div class="form-group">
            <label for="title">Titulo do personagem</label>
            <input type="text" class="form-control"
                   name="title"
                   id="title"
                   required
                   value="{{old('title',$card->title)}}"
            >
        </div>
        <div class="form-group">
            <label for="text_person">Texto do personagem</label>
            <textarea class="form-control"
                name="text_person"
                id="text_person"
                required
                placeholder="Texto do personagem"
            >{{ old('text_person',$card->text_person) }}</textarea>
        </div>
        <div class="form-group">
            <div class="row">
                <img style="width: 150px; height: auto" src="{{url('/storage/'.$card->image)}}" alt="">
            </div>
            <label for="image">Imagem do personagem</label>
            <input type="file" class="form-control"
                   name="image"
                   id="image"
            >
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Atualizar
            </button>
        </div>
    </form>
</div>
@endsection

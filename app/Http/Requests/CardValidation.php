<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CardValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {

                return [
                    'text_person' => 'required|string|max:150',
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'title' => 'required|string|max:30'
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'text_person' => 'string|max:150',
                    'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'title' => 'string|max:20'
                ];
            }
            default:break;
        }
    }
    public function messages()
    {
        return [
            'text_person.required' => 'Um texto para o personagem é requerido',
            'text_person.max' => 'Tamanho máximo 150 caracteres',
            'image.required' => 'Imagem requirida',
            'image.mimes' => 'Formato da imagem inválido',
            'image.max' => 'Tamanho maximo 2 mb',
            'title.required' => 'Titulo requerido',
            'title.max' => 'Tamanho máximo 20 caracteres'
        ];
    }
}

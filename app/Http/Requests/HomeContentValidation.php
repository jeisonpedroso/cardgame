<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HomeContentValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {

                return [];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'text_person' => 'string|max:150',
                    'image_background' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'theme_spotlight' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'title_game' => 'string|max:30',
                    'phrase' => 'string|max:150',
                    'form_desc' => 'string|max:150'
                ];
            }
            default:break;
        }
    }
    public function messages()
    {
        return [
            'text_person.max' => 'Tamanho máximo 150 caracteres',
            'image_background.mimes' => 'Formato da imagem inválido',
            'image_background.max' => 'Tamanho maximo 2 mb',
            'theme_spotlight.mimes' => 'Formato da imagem inválido',
            'theme_spotlight.max' => 'Tamanho maximo 2 mb',
            'title_game.max' => 'Tamanho máximo 30 caracteres',
            'phrase.max' => 'Tamanho máximo 150 caracteres',
            'form_desc.max' => 'Tamanho máximo 150 caracteres'
        ];
    }
}

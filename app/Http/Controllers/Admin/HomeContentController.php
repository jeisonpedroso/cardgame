<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\HomeContent;
use Illuminate\Http\Request;
use App\Http\Requests\HomeContentValidation as HomeContentValidation;

class HomeContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $home_contents = HomeContent::findOrFail($id);

        return view('admin.home_contents.edit',compact('home_contents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HomeContentValidation $request, $id)
    {
        $data = $request->all();
        $home_contents = HomeContent::findOrFail($id);
        if ($request->file('image_background')){
            \File::delete('storage/'.$home_contents->image);
            $path = $request->file('image_background')->store('imagens', 'public');
            $data['image_background'] = $path;
        }
        if ($request->file('theme_spotlight')){
            \File::delete('storage/'.$home_contents->image);
            $path = $request->file('theme_spotlight')->store('imagens', 'public');
            $data['theme_spotlight'] = $path;
        }

        $home_contents->update($data);

        return redirect()->back()->with([
            'type' => 'success',
            'message' => 'Conteúdo atualizado com sucesso'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

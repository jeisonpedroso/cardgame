<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Card;
use Illuminate\Http\Request;
use App\Http\Requests\CardValidation as CardValidation;
class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards = Card::paginate(10);

        return view('admin.cards.index',compact('cards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cards.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CardValidation $request)
    {
        $data = $request->all();
        $path = $request->file('image')->store('imagens', 'public');
        $data['image'] = $path;

        $card = Card::create($data);

        return redirect()->route('admin.cards.index')->with([
            'type' => 'success',
            'message' => 'Card adicionado'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $card = Card::findOrFail($id);

        return view('admin.cards.edit',compact('card'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CardValidation $request, $id)
    {
        $data = $request->all();
        $card = Card::findOrFail($id);
        if (\File::exists('storage/'.$card->image) && $request->file('image')){
            \File::delete('storage/'.$card->image);
            $path = $request->file('image')->store('imagens', 'public');
            $data['image'] = $path;
        }

        $card->update($data);

        return redirect()->route('admin.cards.index')->with([
            'type' => 'success',
            'message' => 'Card atualizado com sucesso'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $card = Card::findOrFail($id);
        if (\File::exists('storage/'.$card->image)){
            \File::delete('storage/'.$card->image);
        }
        $card->delete();

        return redirect()->route('admin.cards.index')->with([
            'type' => 'success',
            'message' => 'Card deletado com sucesso'
        ]);
    }
}

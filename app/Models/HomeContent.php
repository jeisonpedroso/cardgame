<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeContent extends Model
{
    protected $table = 'home_contents';

    protected $fillable = [
        'image_background',
        'title_game',
        'phrase',
        'theme_spotlight',
        'form_desc'
    ];
}

## Instalação

Siga os passos para rodar corretamente a apicação.

1. Após clonado , abrir um bash na pasta do app e rodar o seguinte comando *composer install* para instalar as dependencias do projeto.
2. Criar um banco com nome qualquer e configurar o nome do banco de dados , usuario e senha no arquivo .env ( copiar .env.examplo e renomear para .env)
3. Rodar os seguintes comandos na ordem : php artisan migrate --seed , php artisan key:generate ,php artisan storage:link , php artisan serve.

---

## Acessando admin

1. Acesse **http://localhost:8000/admin** .
2. Usuário e senha criados na seed são : admin e senha 123456
3. Menu Cards pode ser adicionado novo card, editado e deletado.
4. Menu Inicio somente edita o conteúdo da pagina inicial do jogo. 

---

## Considerações

O foco do desenvolvimento foi na regra de negócio do teste , tomei a liberdade de utilizar um tema administrativo com nome de coreUI e também por padrão o laravel já gera as migrates de usuários e toda a rotina básica , então também não alterei nada do tipo.



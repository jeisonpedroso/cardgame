<?php

use Illuminate\Database\Seeder;

class HomeContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('home_contents')->insert([
            'id' => 1,
            'image_background' => 'imagens/default.jpg',
            'title_game'=>'Transistor',
            'phrase'=>'Frase do inicio ',
            'theme_spotlight' =>'imagens/default.jpg',
            'form_desc' => 'Descrição do formulário'
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHomeContents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image_background',150)->comment('patch image background');
            $table->string('title_game',30)->comment('title of the game');
            $table->string('phrase', 150)->comment('phrase home');
            $table->string('theme_spotlight')->comment('Theme spotlight image');
            $table->string('form_desc',150)->comment('form description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_contents');
    }
}
